# Conceptu - Catalogo

### Techs

* [Django] - Biblioteca Python para  Web
* [Django Rest Framework] - Biblioteca Python para APIs 
* [SQLite3] - Simples banco de dados para prototipagem
* [Vue.js] - Microframework javascript

Todo o código está disponível em um repositótio privado no Bitbucket.


### Clone project

```sh
$ git clone https://luigus@bitbucket.org/luigus/conceptu.git 
$ cd conceptu/
$ git checkout develop (se necessário)
```

### Create Docker Image
```sh
$ sudo docker build -t django-image -f Dockerfile .
```
### Run Docker Container
```sh
$ sudo docker run -it -p 8000:8000 django-image
```

### Instalando as dependências usando linha de comando (no caso de não utilizar o Docker)
```sh
$ cd conceptu/ecommerce
$ mkdir .venv
$ pipenv --python 3.7
$ pipenv shell
$ pipenv install
```

### Rodar o Django na máquina local (no caso de não utilizar o Docker)
```sh
$ python manage.py runserver 0.0.0.0:8000
```

### Django super-user (já está criado no arquivo db.sqlite)
* User: admin
* Password: admin

### Django template index.html (com Vue.js)
```sh
$ http://localhost:8000/
```


### API
Lista de chamadas da API:

| API 				   | METHOD 	| ENDPOINTS 								| USER 			| 
| ------ 			   | ------ 	|------ 									|------ 		|
| Lista de categorias  | GET 		| /api/catalogo/categoria/ 					| Any/Admin     | 
| Categoria nome 	   | GET 		| /api/catalogo/categoria/<id_categoria>/ 	| Any/Admin     | 
| Nova categoria 	   | POST 		| /api/catalogo/ 							| Admin 		| 
| Editar categoria 	   | PUT/PATH 	| /api/catalogo/<id_categoria>/ 			| Admin 		| 
| Deletar categoria    | DELETE 	| /api/catalogo/<id_categoria>/ 			| Admin 		| 
| Lista de produtos    | GET 		| /api/catalogo/produto/ 					| Any/Admin     | 
| Descrição de produto | GET 		| /api/catalogo/produto/<id_produto>/ 		| Any/Admin     | 
| Novo produto 		   | POST   	| /api/catalogo/ 							| Admin 		| 
| Editar produto 	   | PUT/PATH 	| /api/catalogo/produto/<id_produto>/ 		| Admin 		| 
| Deletar produto      | DELETE 	| /api/catalogo/produto/<id_produto>/ 		| Admin 		| 

* Any: qualquer usuário

### Todos
 - Implementar unit tests
 - Implementar api tests
 - Checar a segurança da API

License
----

MIT

   
