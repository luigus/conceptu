
import django_filters.rest_framework
from rest_framework import viewsets
from rest_framework import generics

import catalogo.models as m
from .serializers import ProdutoSerializer, CategoriaSerializer
from rest_framework.permissions import IsAuthenticatedOrReadOnly


class ProdutoViewset(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticatedOrReadOnly, )
    queryset = m.Produto.objects.all()
    serializer_class = ProdutoSerializer


class CategoriaViewset(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticatedOrReadOnly, )
    queryset = m.Categoria.objects.all()
    serializer_class = CategoriaSerializer


class ProdutoListView(generics.ListAPIView):
    queryset = m.Produto.objects.all()
    serializer_class = ProdutoSerializer
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]
