
from django.urls import path, include
from .views import ProdutoViewset, CategoriaViewset
from rest_framework import routers

routers = routers.DefaultRouter()
routers.register('produto', ProdutoViewset)
routers.register('categoria', CategoriaViewset)

urlpatterns = [
    path('catalogo/', include(routers.urls))
]