
from rest_framework import serializers
import catalogo.models as m


class CategoriaSerializer(serializers.ModelSerializer):
    class Meta:
        model = m.Categoria
        fields = ['id', 'nome']


class ProdutoSerializer(serializers.ModelSerializer):

    categoria = CategoriaSerializer(read_only=True)

    class Meta:
        model = m.Produto
        fields = ['nome', 'publicado_em', 'modificado_em', 'descricao', 'preco', 'imagem', 'categoria']


