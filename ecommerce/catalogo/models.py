from django.db import models


class Categoria(models.Model):
    
    nome = models.CharField(max_length=255)
    
    def __str__(self):
        return self.nome
            
    class Meta:
        verbose_name = 'Categoria'
        verbose_name_plural = 'Categorias'


class Produto(models.Model):
    
    nome = models.CharField(max_length=255)
    publicado_em = models.DateField(auto_now=True)
    modificado_em = models.DateField(auto_now=True)
    descricao = models.TextField(null=True, blank=True)
    preco = models.DecimalField(max_digits=10, decimal_places=2)
    categoria = models.ForeignKey(Categoria, on_delete=models.CASCADE)
    imagem = models.ImageField(upload_to='produto', blank=True, null=True)
    
    def __str__(self):
        return self.nome
            
    class Meta:
        verbose_name = 'Produto'
        verbose_name_plural = 'Produtos'
