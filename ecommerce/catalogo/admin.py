from django.contrib import admin

# Register your models here.
from .models import Produto, Categoria


class ProdutoAdmin(admin.ModelAdmin):
    list_display = ('nome', 'descricao', 'publicado_em', 'modificado_em')

    class Meta:
        model = Produto


admin.site.register(Produto, ProdutoAdmin)
admin.site.register(Categoria)
