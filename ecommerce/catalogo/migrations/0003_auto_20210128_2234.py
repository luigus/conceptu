# Generated by Django 3.1.5 on 2021-01-28 22:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalogo', '0002_produto_imagem'),
    ]

    operations = [
        migrations.AddField(
            model_name='produto',
            name='modificado_em',
            field=models.DateField(auto_now=True),
        ),
        migrations.AddField(
            model_name='produto',
            name='publicado_em',
            field=models.DateField(auto_now=True),
        ),
    ]
